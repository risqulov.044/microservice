package uz.ilxom.services.gateway;


import org.springdoc.core.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionLocator;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

@Configuration
@RestController
public class SwaggerController {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.bank.ofb.solabuswebservice.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    @Autowired
    RouteDefinitionLocator locator;

    @Bean
    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r
                        .path("/api/history/**")
                        .uri("http://localhost:8080/"))

                .route(r -> r
                        .path("/api/car/**")
                        .uri("http://localhost:8081/"))

                .route(r -> r
                        .path("/api/user/**")
                        .uri("http://localhost:8082/"))

                .build();
    }

//    @Bean
//    GroupedOpenApi userApis(){
//        return GroupedOpenApi.builder().group("user").pathsToMatch("/**/user/**").build();
//    }

    @Bean
    public List<GroupedOpenApi> apis() {
        List<GroupedOpenApi> groups = new ArrayList<>();
        List<RouteDefinition> definitions = locator.getRouteDefinitions().collectList().block();
        assert definitions != null;
        definitions.stream().filter(routeDefinition -> routeDefinition.getId().matches(".*-service")).forEach(routeDefinition -> {
            String name = routeDefinition.getId().replaceAll("-service", "");
            groups.add(GroupedOpenApi.builder().pathsToMatch("/" + name + "/**").group(name).build());
        });
        return groups;
    }

    @Bean
    GroupedOpenApi studentApis() {
        return GroupedOpenApi.builder()
                .group("history")
                .pathsToExclude("/api/history/**", "/history/**")
                .pathsToMatch("/api/history/**", "/history/**")
                .build();
    }

    @Bean
    GroupedOpenApi groupApis() {
        return GroupedOpenApi.builder()
                .group("car")
                .pathsToExclude("/api/car/**", "/car/**")
                .pathsToMatch("/api/car/**", "/car/**")
                .build();
    }

    @Bean
    GroupedOpenApi authApis() {
        return GroupedOpenApi.builder()
                .group("user")
                .pathsToExclude("/api/user/**", "/user/**")
                .pathsToMatch("/api/user/**", "/user/**")
                .build();
    }

}
