package uz.ilxom.services.history.payload;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Setter
@Getter
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class CarDTO implements Serializable{
    private String brand;
    private String stateNumber;
    private Double mileage;
    private Long drivingTime;
}
