package uz.ilxom.services.history.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.ilxom.services.history.service.HistoryService;

import java.util.UUID;

import static uz.ilxom.services.history.responsedata.ResponseData.ok;
import static uz.ilxom.services.history.utils.enums.Code.SUCCESS;


@RestController
@RequestMapping("/api/history")
@RequiredArgsConstructor
public class HistoryController {

    private final HistoryService historyService;


    @GetMapping
    public ResponseEntity<?> getHistories() {
        return ok(SUCCESS, historyService.getAllHistory());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getHistory(@PathVariable UUID id) {
        return ok(SUCCESS, historyService.getHistory(id));
    }
}
