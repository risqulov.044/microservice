package uz.ilxom.services.history.rabbitListener;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import uz.ilxom.services.history.payload.request.HistoryRequest;
import uz.ilxom.services.history.service.HistoryService;


@Log4j2
@Component
@RequiredArgsConstructor
public class UserListener {

    @Autowired
    private HistoryService historyService;


    @RabbitListener(queues = "user.update.queue", errorHandler = "rabbitErrorHandler")
    public void updateUserData(HistoryRequest result) {
        historyService.save(result);
    }

}
