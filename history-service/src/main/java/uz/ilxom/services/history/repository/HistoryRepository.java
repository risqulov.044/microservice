package uz.ilxom.services.history.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.ilxom.services.history.entity.History;
import uz.ilxom.services.history.utils.enums.Status;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface HistoryRepository extends JpaRepository<History, UUID> {

    Optional<History> findByIdAndStatus(UUID id, Status status);
}
