package uz.ilxom.services.history.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.ilxom.services.history.entity.History;
import uz.ilxom.services.history.exception.BadRequestAlertException;
import uz.ilxom.services.history.mapper.HistoryMapper;
import uz.ilxom.services.history.payload.AddressDTO;
import uz.ilxom.services.history.payload.request.HistoryRequest;
import uz.ilxom.services.history.payload.response.HistoryResponse;
import uz.ilxom.services.history.repository.HistoryRepository;
import uz.ilxom.services.history.service.HistoryService;
import uz.ilxom.services.history.utils.enums.Status;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class HistoryServiceImpl implements HistoryService {

    private final HistoryRepository historyRepository;
    private final HistoryMapper historyMapper;


    @Override
    public List<HistoryResponse> getAllHistory() {
        return historyRepository.findAll().stream()
                .map(historyMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public HistoryResponse getHistory(UUID id) {
        return historyMapper.toDTO(historyRepository.findByIdAndStatus(id, Status.ACTIVE)
                .orElseThrow(BadRequestAlertException::historyNotFound));
    }

    @Override
    public void save(HistoryRequest historyRequest) {

        History history = History.builder()
                .distanceTraveled(String.valueOf(historyRequest.getAddresses().stream()
                        .map(AddressDTO::getDistance).toList()))
                .addressName(String.valueOf(historyRequest.getAddresses().stream()
                        .map(AddressDTO::getAddressName).toList()))
                .totalDistanceTraveled(historyRequest.getAddresses().stream()
                        .mapToDouble(AddressDTO::getDistance).sum())
                .brand(historyRequest.getCar().getBrand())
                .stateNumber(historyRequest.getCar().getStateNumber())
                .oldCarMileage(historyRequest.getCar().getMileage())
                .newCarMileage(historyRequest.getCar().getMileage() + historyRequest.getAddresses().stream()
                        .mapToDouble(AddressDTO::getDistance).sum())
                .dateTime(historyRequest.getCar().getDrivingTime())
                .firstName(historyRequest.getFirstName())
                .lastName(historyRequest.getLastName())
                .phoneNumber(historyRequest.getPhoneNumber())
                .status(Status.ACTIVE)
                .build();
        historyRepository.save(history);
    }
}
