package uz.ilxom.services.history.message;

import lombok.Getter;

@Getter
public final class MessageKey {

    public static final String HISTORY_NOT_FOUND = "history.not.found";
    public static final String PARAMETERS_NOT_VALID = "params.not.valid";
    public static final String SUCCESS_MESSAGE = "success.message";
    public MessageKey() {

    }
}
