package uz.ilxom.services.history.mapper;

import org.mapstruct.Mapper;
import uz.ilxom.services.history.entity.History;
import uz.ilxom.services.history.payload.response.HistoryResponse;

@Mapper(componentModel = "spring", imports = {HistoryResponse.class, History.class})
public interface HistoryMapper {


    HistoryResponse toDTO(History history);
}
