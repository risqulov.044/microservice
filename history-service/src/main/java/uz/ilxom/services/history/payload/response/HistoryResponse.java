package uz.ilxom.services.history.payload.response;

import lombok.*;
import uz.ilxom.services.history.utils.enums.Status;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class HistoryResponse {
    private UUID id;

    private String distanceTraveled;
    private Double totalDistanceTraveled;
    private String stateNumber;
    private Double oldCarMileage;
    private Double newCarMileage;
    private Long dateTime;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String brand;
    private String addressName;
    private Status status;
}
