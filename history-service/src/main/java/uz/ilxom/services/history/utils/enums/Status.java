package uz.ilxom.services.history.utils.enums;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Optional;

@Getter
public enum Status {

    ACTIVE,
    DELETED;

    public static final Status[] values = Status.values();

    public static Optional<Status> findByName(String s) {
        return Arrays.stream(values)
                .filter(st -> StringUtils.equalsIgnoreCase(st.name(), s))
                .findFirst();
    }
}
