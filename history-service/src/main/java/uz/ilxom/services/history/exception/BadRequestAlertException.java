package uz.ilxom.services.history.exception;

import lombok.Getter;
import uz.ilxom.services.history.utils.enums.Code;

import static uz.ilxom.services.history.message.MessageKey.HISTORY_NOT_FOUND;
import static uz.ilxom.services.history.message.MessageKey.PARAMETERS_NOT_VALID;
import static uz.ilxom.services.history.utils.enums.Code.DATA_NOT_FOUND;
import static uz.ilxom.services.history.utils.enums.Code.INVALID_DATA;


@Getter
public class BadRequestAlertException extends RuntimeException {

    private final Code code;

    public BadRequestAlertException(String message, Code code) {
        super(message);
        this.code = code;
    }

    public static BadRequestAlertException badRequestAlertException(String key, Code code) {
        return new BadRequestAlertException(key, code);
    }

    public static BadRequestAlertException historyNotFound() {
        return badRequestAlertException(HISTORY_NOT_FOUND, DATA_NOT_FOUND);
    }


    public static BadRequestAlertException parameterNotValid() {
        return new BadRequestAlertException(PARAMETERS_NOT_VALID, INVALID_DATA);
    }
}
