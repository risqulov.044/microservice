package uz.ilxom.services.history.payload.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;
import uz.ilxom.services.history.payload.AddressDTO;
import uz.ilxom.services.history.payload.CarDTO;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HistoryRequest implements Serializable{

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private Byte age;
    private CarDTO car;
    private List<AddressDTO> addresses;
}
