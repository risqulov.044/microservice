package uz.ilxom.services.history.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;
import uz.ilxom.services.history.utils.enums.Status;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "history")
public class History {

    @Transient
    public static final String sequenceName = "history_id_seq";

    @Id
    @GeneratedValue(generator = sequenceName, strategy = GenerationType.SEQUENCE)
    @GenericGenerator(
            name = sequenceName,
            strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    private String distanceTraveled;
    private Double totalDistanceTraveled;
    private String stateNumber;
    private Double oldCarMileage;
    private Double newCarMileage;
    private Long dateTime;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String brand;
    private String addressName;
    @Enumerated(EnumType.STRING)
    private Status status;
}
