package uz.ilxom.services.history.service;

import uz.ilxom.services.history.payload.request.HistoryRequest;
import uz.ilxom.services.history.payload.response.HistoryResponse;

import java.util.List;
import java.util.UUID;

public interface HistoryService {

    List<HistoryResponse> getAllHistory();

    HistoryResponse getHistory(UUID id);


    void save(HistoryRequest historyRequest);
}
