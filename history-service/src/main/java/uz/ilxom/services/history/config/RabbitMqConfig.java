package uz.ilxom.services.history.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableRabbit
@Configuration
public class RabbitMqConfig {

    public static final String USER_UPDATE_ROUTE = "user.update.route";
    public static final String USER_UPDATE_EXCHANGE = "user.update.topic.exchange";
    public static final String USER_UPDATE_QUEUE = "user.update.queue";


    @Bean(USER_UPDATE_QUEUE)
    public Queue userUpdateQueue() {
        return new Queue(USER_UPDATE_QUEUE, false);
    }

    @Bean(USER_UPDATE_ROUTE)
    public Binding updateDataBinding(@Qualifier(USER_UPDATE_QUEUE) Queue queue,
                                     @Qualifier(USER_UPDATE_EXCHANGE) TopicExchange exchange) {
        return BindingBuilder
                .bind(queue)
                .to(exchange)
                .with(USER_UPDATE_ROUTE);
    }

    @Bean(USER_UPDATE_EXCHANGE)
    public TopicExchange updateDateExchange() {
        return new TopicExchange(USER_UPDATE_EXCHANGE);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(messageConverter());
        return rabbitTemplate;
    }

    @Bean(name = "jacksonMessageConverter")
    public Jackson2JsonMessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
