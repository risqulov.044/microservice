package uz.ilxom.services.car.service;

import uz.ilxom.services.car.entrity.Car;
import uz.ilxom.services.car.payload.MessageInfo;
import uz.ilxom.services.car.payload.request.CarRequest;
import uz.ilxom.services.car.payload.response.CarResponse;

import java.util.List;
import java.util.UUID;

public interface CarService {

    List<CarResponse> getAllCar();

    CarResponse getCar(UUID id);
//
//    CarResponse save(CarRequest carRequest);
//
//    CarResponse update(CarRequest userRequest, UUID id);
//
//    MessageInfo delete(UUID id);

    Car saveDefault(Car car);
}
