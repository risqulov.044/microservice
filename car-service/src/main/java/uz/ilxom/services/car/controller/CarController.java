package uz.ilxom.services.car.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.ilxom.services.car.service.CarService;

import java.util.UUID;

import static uz.ilxom.services.car.common.responsedata.ResponseData.ok;
import static uz.ilxom.services.car.utils.enums.Code.SUCCESS;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/car")
public class CarController {

    private final CarService carService;


    @GetMapping
    public ResponseEntity<?> getUsers() {
        return ok(SUCCESS, carService.getAllCar());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUser(@PathVariable UUID id) {
        return ok(SUCCESS, carService.getCar(id));
    }
}
