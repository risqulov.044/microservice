package uz.ilxom.services.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.ilxom.services.car.entrity.Car;
import uz.ilxom.services.car.utils.enums.Status;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface CarRepository extends JpaRepository<Car, UUID> {

    Optional<Car> findByUserIdAndStatus(UUID id, Status status);
}
