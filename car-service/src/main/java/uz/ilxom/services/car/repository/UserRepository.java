package uz.ilxom.services.car.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.ilxom.services.car.entrity.User;
import uz.ilxom.services.car.utils.enums.Status;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

    Optional<User> findByIdAndStatus(UUID id, Status status);
}
