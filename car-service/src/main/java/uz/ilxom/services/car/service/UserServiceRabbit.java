package uz.ilxom.services.car.service;

import uz.ilxom.services.car.payload.response.UserResponseRabbit;

public interface UserServiceRabbit {

    void sendResult(UserResponseRabbit userResponseRabbit);
}
