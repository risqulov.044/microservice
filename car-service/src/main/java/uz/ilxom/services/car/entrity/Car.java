package uz.ilxom.services.car.entrity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.GenericGenerator;
import uz.ilxom.services.car.utils.enums.Status;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "cars")
public class Car {

    @Transient
    public static final String sequenceName = "car_id_seq";

    @Id
    @GeneratedValue(generator = sequenceName, strategy = GenerationType.SEQUENCE)
    @GenericGenerator(
            name = sequenceName,
            strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    private String brand;
    private String stateNumber;
    private Double mileage;
    private Long drivingTime;
    @Enumerated(EnumType.STRING)
    private Status status;

    @JsonBackReference(value = "user_id")
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
}
