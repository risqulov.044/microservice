package uz.ilxom.services.car.mapper;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import uz.ilxom.services.car.entrity.User;
import uz.ilxom.services.car.payload.request.UserRequest;
import uz.ilxom.services.car.payload.response.UserResponse;
import uz.ilxom.services.car.utils.enums.Status;

import java.util.UUID;

@Mapper(componentModel = "spring", imports = {UserResponse.class, UserRequest.class, User.class})
public interface UsersMapper {


    UserResponse toDTO(User user);

    @Mapping(target = "status", expression = "java(status)")
    @Mapping(target = "id", expression = "java(id)")
    User toEntity(UserRequest userRequest, @Context UUID id, @Context Status status);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "status", expression = "java(status)")
    User create(UserRequest userRequest, @Context Status status);
}
