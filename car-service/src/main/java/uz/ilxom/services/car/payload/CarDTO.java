package uz.ilxom.services.car.payload;

import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class CarDTO implements Serializable {
    private String brand;
    private String stateNumber;
    private Long drivingTime;
    private Double mileage;
}
