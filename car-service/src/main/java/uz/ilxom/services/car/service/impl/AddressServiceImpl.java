package uz.ilxom.services.car.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.ilxom.services.car.entrity.Address;
import uz.ilxom.services.car.exception.BadRequestAlertException;
import uz.ilxom.services.car.mapper.AddressMapper;
import uz.ilxom.services.car.payload.response.AddressResponse;
import uz.ilxom.services.car.repository.AddressRepository;
import uz.ilxom.services.car.service.AddressService;
import uz.ilxom.services.car.utils.enums.Status;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;
    private final AddressMapper addressMapper;


    @Override
    public List<AddressResponse> getAllAddresses() {
        return addressRepository.findAll()
                .stream()
                .map(addressMapper::toDTO)
                .toList();
    }

    @Override
    public AddressResponse getAddress(UUID id) {
        return addressRepository.findByIdAndStatus(id, Status.ACTIVE)
                .map(addressMapper::toDTO)
                .orElseThrow(BadRequestAlertException::addressNotFound);
    }

    @Override
    public Address saveDefault(Address address) {
        return addressRepository.save(address);
    }
}
