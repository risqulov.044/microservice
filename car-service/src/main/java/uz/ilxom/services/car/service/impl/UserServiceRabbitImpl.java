package uz.ilxom.services.car.service.impl;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import uz.ilxom.services.car.payload.response.UserResponseRabbit;
import uz.ilxom.services.car.service.UserServiceRabbit;

@Log4j2
@Component
@RequiredArgsConstructor
public class UserServiceRabbitImpl implements UserServiceRabbit {
    public static final String USER_UPDATE_ROUTE = "user.update.route";
    public static final String USER_UPDATE_EXCHANGE = "user.update.topic.exchange";
    public static final String USER_UPDATE_QUEUE = "user.update.queue";
    private final RabbitTemplate rabbitTemplate;

    @Override
    public void sendResult(UserResponseRabbit userResponseRabbit) {
        log.debug("SEND MESSAGE = {} EXCHANGE = {} ROUTE_KEY = {}", userResponseRabbit, USER_UPDATE_EXCHANGE, USER_UPDATE_QUEUE);
        Try.run(() -> rabbitTemplate.convertAndSend(USER_UPDATE_EXCHANGE, USER_UPDATE_ROUTE, userResponseRabbit))
                .onFailure(log::error);
    }
}
