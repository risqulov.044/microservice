package uz.ilxom.services.car.mapper;

import org.mapstruct.Mapper;
import uz.ilxom.services.car.entrity.Address;
import uz.ilxom.services.car.payload.response.AddressResponse;

@Mapper(componentModel = "spring", imports = {AddressResponse.class, Address.class})
public interface AddressMapper {


    AddressResponse toDTO(Address address);
}
