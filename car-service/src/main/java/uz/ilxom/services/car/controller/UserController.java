package uz.ilxom.services.car.controller;

import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.ilxom.services.car.payload.request.UserRequest;
import uz.ilxom.services.car.service.UserService;

import java.util.UUID;

import static uz.ilxom.services.car.common.responsedata.ResponseData.ok;
import static uz.ilxom.services.car.utils.enums.ApiConstant.API_CAR;
import static uz.ilxom.services.car.utils.enums.Code.SUCCESS;

@RestController
@RequestMapping(value = API_CAR + "/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;


    @GetMapping
    public ResponseEntity<?> getUsers() {
        return ok(SUCCESS, userService.getAllUser());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUser(@PathVariable UUID id) {
        return ok(SUCCESS, userService.getUser(id));
    }

    @PostMapping
    public ResponseEntity<?> saveUser(@RequestBody UserRequest userRequest) {
        return ok(SUCCESS, userService.add(userRequest));
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> updateUser(@RequestBody UserRequest userRequest, @PathVariable UUID id) {
        return ok(SUCCESS, userService.update(userRequest, id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable UUID id) {
        return ok(SUCCESS, userService.delete(id));
    }
}
