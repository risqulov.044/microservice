package uz.ilxom.services.car.mapper;

import org.mapstruct.Mapper;
import uz.ilxom.services.car.entrity.Car;
import uz.ilxom.services.car.payload.request.CarRequest;
import uz.ilxom.services.car.payload.response.CarResponse;

@Mapper(componentModel = "spring", imports = {CarResponse.class, CarRequest.class, Car.class})
public interface CarMapper {


    CarResponse toDTO(Car user);
}
