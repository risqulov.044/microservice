package uz.ilxom.services.car.payload.response;

import lombok.*;
import uz.ilxom.services.car.entrity.Address;
import uz.ilxom.services.car.entrity.Car;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {

    private UUID id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private Byte age;
    private Car car;
    private List<Address> addresses;
}
