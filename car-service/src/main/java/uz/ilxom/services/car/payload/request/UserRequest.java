package uz.ilxom.services.car.payload.request;

import lombok.*;
import uz.ilxom.services.car.entrity.Address;
import uz.ilxom.services.car.entrity.Car;

import java.util.List;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private Byte age;
    private Car car;
    private List<Address> addresses;
}
