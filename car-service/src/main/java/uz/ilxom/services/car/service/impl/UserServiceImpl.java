package uz.ilxom.services.car.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.ilxom.services.car.entrity.Address;
import uz.ilxom.services.car.entrity.Car;
import uz.ilxom.services.car.entrity.User;
import uz.ilxom.services.car.exception.BadRequestAlertException;
import uz.ilxom.services.car.mapper.UsersMapper;
import uz.ilxom.services.car.payload.AddressDTO;
import uz.ilxom.services.car.payload.CarDTO;
import uz.ilxom.services.car.payload.MessageInfo;
import uz.ilxom.services.car.payload.request.UserRequest;
import uz.ilxom.services.car.payload.response.UserResponse;
import uz.ilxom.services.car.payload.response.UserResponseRabbit;
import uz.ilxom.services.car.repository.UserRepository;
import uz.ilxom.services.car.service.AddressService;
import uz.ilxom.services.car.service.CarService;
import uz.ilxom.services.car.service.UserService;
import uz.ilxom.services.car.service.UserServiceRabbit;
import uz.ilxom.services.car.utils.enums.Status;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static io.vavr.control.Option.ofOptional;
import static uz.ilxom.services.car.message.MessageKey.SUCCESS_MESSAGE;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UsersMapper usersMapper;
    private final UserServiceRabbit userServiceRabbit;

    private final CarService carService;

    private final AddressService addressService;


    @Override
    public List<UserResponse> getAllUser() {
        return userRepository.findAll().stream()
                .map(usersMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public UserResponse getUser(UUID id) {
        return userRepository.findByIdAndStatus(id, Status.ACTIVE)
                .map(usersMapper::toDTO)
                .orElseThrow(BadRequestAlertException::userNotFound);
    }

    @Override
    public UserResponse add(UserRequest userRequest) {

        User user = saveDefault(User.builder()
                .firstName(userRequest.getFirstName())
                .lastName(userRequest.getLastName())
                .phoneNumber(userRequest.getPhoneNumber())
                .age(userRequest.getAge())
                .status(Status.ACTIVE)
                .build());
        Car car = userRequest.getCar();

        car = Car.builder()
                .drivingTime(System.currentTimeMillis())
                .user(user)
                .mileage(car.getMileage())
                .stateNumber(car.getStateNumber())
                .brand(car.getBrand())
                .status(Status.ACTIVE)
                .build();

        Car car1 = carService.saveDefault(car);

        List<Address> addresses = userRequest.getAddresses().parallelStream()
                .map(address -> addressService.saveDefault(Address.builder()
                        .addressName(address.getAddressName())
                        .distance(address.getDistance())
                        .user(user)
                        .status(Status.ACTIVE)
                        .build()))
                .toList();

        user.setCar(car1);
        user.setAddresses(addresses);

        UserResponse userResponse = usersMapper.toDTO(user);
        UserResponseRabbit responseRabbit = UserResponseRabbit.builder()
                .firstName(userResponse.getFirstName())
                .lastName(userResponse.getLastName())
                .phoneNumber(userResponse.getPhoneNumber())
                .age(userResponse.getAge())
                .car(CarDTO.builder()
                        .brand(userResponse.getCar().getBrand())
                        .stateNumber(userResponse.getCar().getStateNumber())
                        .mileage(userResponse.getCar().getMileage())
                        .drivingTime(userResponse.getCar().getDrivingTime())
                        .build())
                .addresses(addresses.stream().map(address -> AddressDTO.builder()
                        .addressName(address.getAddressName())
                        .distance(address.getDistance())
                        .build()).collect(Collectors.toList()))
                .build();


        userServiceRabbit.sendResult(responseRabbit);

        return userResponse;
    }

    @Override
    public UserResponse save(UserRequest userRequest) {
        return usersMapper.toDTO(userRepository.save(usersMapper.create(userRequest, Status.ACTIVE)));
    }

    @Override
    public UserResponse update(UserRequest userRequest, UUID id) {
        userRepository.findById(id).orElseThrow(BadRequestAlertException::userNotFound);
        User user = usersMapper.toEntity(userRequest, id, Status.ACTIVE);
        return usersMapper.toDTO(userRepository.save(user));
    }

    @Override
    public MessageInfo delete(UUID id) {
        ofOptional(userRepository.findByIdAndStatus(id, Status.ACTIVE))
                .peek(user -> user.setStatus(Status.DELETED))
                .peek(userRepository::save)
                .getOrElseThrow(BadRequestAlertException::userNotFound);
        return new MessageInfo(SUCCESS_MESSAGE);
    }

    @Override
    public User saveDefault(User user) {
        return userRepository.save(user);
    }

}
