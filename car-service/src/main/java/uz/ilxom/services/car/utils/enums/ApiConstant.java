package uz.ilxom.services.car.utils.enums;

import lombok.Getter;

@Getter
public class ApiConstant {
    public static final String API_CAR = "/api/car";

}
