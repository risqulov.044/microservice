package uz.ilxom.services.car.payload;

import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class AddressDTO implements Serializable {
    private String addressName;
    private Double distance;
}
