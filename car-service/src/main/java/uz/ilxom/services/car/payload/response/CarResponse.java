package uz.ilxom.services.car.payload.response;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CarResponse {

    private UUID id;
    private String brand;
    private String stateNumber;
    private Double mileage;
}
