package uz.ilxom.services.car.service;

import uz.ilxom.services.car.entrity.Address;
import uz.ilxom.services.car.payload.response.AddressResponse;

import java.util.List;
import java.util.UUID;

public interface AddressService {

    List<AddressResponse> getAllAddresses();

    AddressResponse getAddress(UUID id);

    Address saveDefault(Address address);
}
