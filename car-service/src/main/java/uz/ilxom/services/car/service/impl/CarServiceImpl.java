package uz.ilxom.services.car.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.ilxom.services.car.entrity.Car;
import uz.ilxom.services.car.exception.BadRequestAlertException;
import uz.ilxom.services.car.mapper.CarMapper;
import uz.ilxom.services.car.payload.MessageInfo;
import uz.ilxom.services.car.payload.request.CarRequest;
import uz.ilxom.services.car.payload.response.CarResponse;
import uz.ilxom.services.car.repository.CarRepository;
import uz.ilxom.services.car.service.CarService;
import uz.ilxom.services.car.utils.enums.Status;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static io.vavr.control.Option.ofOptional;
import static uz.ilxom.services.car.message.MessageKey.SUCCESS_MESSAGE;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;
    private final CarMapper carMapper;


    @Override
    public List<CarResponse> getAllCar() {
        return carRepository.findAll().stream()
                .map(carMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public CarResponse getCar(UUID id) {
        return carRepository.findByUserIdAndStatus(id, Status.ACTIVE)
                .map(carMapper::toDTO)
                .orElseThrow(BadRequestAlertException::carNotFound);
    }

    @Override
    public Car saveDefault(Car car) {
        return carRepository.save(car);
    }
}
