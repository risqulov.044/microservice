package uz.ilxom.services.car.payload.response;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AddressResponse {

    private UUID id;
    private String addressName;
    private Double distance;
}
