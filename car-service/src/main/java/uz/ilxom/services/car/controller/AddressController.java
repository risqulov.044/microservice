package uz.ilxom.services.car.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.ilxom.services.car.service.AddressService;

import java.util.UUID;

import static uz.ilxom.services.car.common.responsedata.ResponseData.ok;
import static uz.ilxom.services.car.utils.enums.Code.SUCCESS;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/car/address")
public class AddressController {

    private final AddressService addressService;


    @GetMapping
    public ResponseEntity<?> getUsers() {
        return ok(SUCCESS, addressService.getAllAddresses());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUser(@PathVariable UUID id) {
        return ok(SUCCESS, addressService.getAddress(id));
    }
}
