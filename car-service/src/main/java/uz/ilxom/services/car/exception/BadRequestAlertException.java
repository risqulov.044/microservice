package uz.ilxom.services.car.exception;

import lombok.Getter;
import uz.ilxom.services.car.utils.enums.Code;

import static uz.ilxom.services.car.message.MessageKey.*;
import static uz.ilxom.services.car.utils.enums.Code.DATA_NOT_FOUND;
import static uz.ilxom.services.car.utils.enums.Code.INVALID_DATA;


@Getter
public class BadRequestAlertException extends RuntimeException {

    private final Code code;

    public BadRequestAlertException(String message, Code code) {
        super(message);
        this.code = code;
    }

    public static BadRequestAlertException badRequestAlertException(String key, Code code) {
        return new BadRequestAlertException(key, code);
    }

    public static BadRequestAlertException carNotFound() {
        return badRequestAlertException(CAR_NOT_FOUND, DATA_NOT_FOUND);
    }

    public static BadRequestAlertException addressNotFound() {
        return badRequestAlertException(ADDRESS_NOT_FOUND, DATA_NOT_FOUND);
    }

    public static BadRequestAlertException userNotFound() {
        return badRequestAlertException(USER_NOT_FOUND, DATA_NOT_FOUND);
    }

    public static BadRequestAlertException parameterNotValid() {
        return new BadRequestAlertException(PARAMETERS_NOT_VALID, INVALID_DATA);
    }
}
