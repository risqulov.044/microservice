package uz.ilxom.services.car.payload.request;

import lombok.*;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CarRequest {

    private String brand;
    private String stateNumber;
    private Double mileage;
}
