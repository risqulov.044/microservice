package uz.ilxom.services.car.payload.response;

import lombok.*;
import uz.ilxom.services.car.payload.AddressDTO;
import uz.ilxom.services.car.payload.CarDTO;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseRabbit implements Serializable {

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private Byte age;
    private CarDTO car;
    private List<AddressDTO> addresses;
}
