package uz.ilxom.services.car.common.responsedata;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;
import org.springframework.http.ResponseEntity;
import uz.ilxom.services.car.utils.enums.Code;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"code", "data", "errorMessage", "timestamp"})
public class ResponseData<T> {

    private Code code;

    private T data;

    private String errorMessage;

    private Long timestamp;

    public static <T> ResponseData<T> responseData(Code code, T t) {
        return new ResponseData<>(code, t, null, System.currentTimeMillis());
    }

    public static ResponseData<?> errorResponseData(Code code, String key) {
        return new ResponseData<>(code, null, key, System.currentTimeMillis());
    }

    public static <T> ResponseEntity<ResponseData<T>> ok(Code code, T t) {
        return ResponseEntity.ok(ResponseData.responseData(code, t));
    }
}
