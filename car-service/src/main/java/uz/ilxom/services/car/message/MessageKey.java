package uz.ilxom.services.car.message;

import lombok.Getter;

@Getter
public final class MessageKey {

    public static final String CAR_NOT_FOUND = "car.not.found";
    public static final String ADDRESS_NOT_FOUND = "address.not.found";
    public static final String USER_NOT_FOUND = "user.not.found";

    public static final String PARAMETERS_NOT_VALID = "params.not.valid";

    public static final String SUCCESS_MESSAGE = "success.message";
    public MessageKey() {

    }
}
