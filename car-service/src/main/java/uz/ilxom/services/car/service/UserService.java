package uz.ilxom.services.car.service;

import uz.ilxom.services.car.entrity.User;
import uz.ilxom.services.car.payload.MessageInfo;
import uz.ilxom.services.car.payload.request.UserRequest;
import uz.ilxom.services.car.payload.response.UserResponse;

import java.util.List;
import java.util.UUID;

public interface UserService {

    List<UserResponse> getAllUser();

    UserResponse getUser(UUID id);

    UserResponse save(UserRequest userRequest);

    UserResponse add(UserRequest userRequest);

    UserResponse update(UserRequest userRequest, UUID id);

    MessageInfo delete(UUID id);

    User saveDefault(User user);

}
